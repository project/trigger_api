<?php

namespace Drupal\trigger_api\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "CKEditor" plugin.
 *
 * @CKEditorPlugin(
 *   id = "trigger_api_ckeditor",
 *   label = @Translation("CKEditor"),
 *   module = "trigger_api"
 * )
 */
class Ckeditor extends CKEditorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Trigger api file path.
   *
   * @var string
   */
  protected string $triggerApiPath;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list
   *   The extension list.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ModuleExtensionList $extension_list) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->triggerApiPath = $extension_list->getPath('trigger_api');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('extension.list.module'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->triggerApiPath . '/js/plugins/ckeditor/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $module_path = $this->triggerApiPath;
    return [
      'ckeditor' => [
        'label' => $this->t('CKEditor'),
        'image' => $module_path . '/js/plugins/ckeditor/icons/ckeditor.png',
      ],
    ];
  }

}
