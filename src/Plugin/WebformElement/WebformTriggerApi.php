<?php

namespace Drupal\trigger_api\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\trigger_api\Form\Traits\TriggerAPIFormValidatorTrait;
use Drupal\webform\Plugin\WebformElementBase;

/**
 * Provides a 'webform_example_element' element.
 *
 * @WebformElement(
 *   id = "webform_trigger_api",
 *   label = @Translation("Trigger API"),
 *   description = @Translation("Provides a webform element for trigger api."),
 *   category = @Translation("Trigger"),
 * )
 *
 * @see \Drupal\webform_example_element\Element\WebformExampleElement
 * @see \Drupal\webform\Plugin\WebformElementBase
 * @see \Drupal\webform\Plugin\WebformElementInterface
 * @see \Drupal\webform\Annotation\WebformElement
 */
class WebformTriggerApi extends WebformElementBase {

  use TriggerAPIFormValidatorTrait;

  /**
   * FIELD Type.
   *
   * @const string
   */
  public const FIELD_TYPE = 'trigger_type';

  /**
   * Field data.
   *
   * @const string
   */
  public const FIELD_DATA = 'trigger_data';

  /**
   * Field trigger event.
   *
   * @const string
   */
  public const FIELD_EVENT_TYPE = 'trigger_event_type';

  /**
   * Event Type error.
   *
   * @const string
   */
  public const EVENT_TYPE_ERROR = 'trigger_event_type_error';

  /**
   * Event type no error.
   *
   * @const string
   */
  public const EVENT_TYPE_NO_ERROR = 'trigger_event_type_no_error';

  /**
   * Event type show.
   *
   * @const string
   */
  public const EVENT_TYPE_ELEMENT_SHOW = 'trigger_event_type_element_show';

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'multiple'               => FALSE,
      static::FIELD_TYPE       => '',
      static::FIELD_DATA       => '',
      static::FIELD_EVENT_TYPE => static::EVENT_TYPE_NO_ERROR,
    ]
    + parent::defineDefaultProperties();
  }

  /**
   * {@inheritdoc}
   */
  protected function defineTranslatableProperties() {
    return array_merge(
      parent::defineTranslatableProperties(),
      ['null_label', static::FIELD_DATA],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function supportsMultipleValues() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $form['element']['multiple']['#access'] = FALSE;

    $form[static::FIELD_TYPE] = [
      '#type'     => 'textfield',
      '#title'    => $this->t('Type'),
      '#required' => TRUE,
    ];

    $form[static::FIELD_DATA] = [
      '#type'             => 'textarea',
      '#title'            => $this->t('JSON data'),
      '#required'         => TRUE,
      '#element_validate' => [[$this, 'validateJson']],
    ];

    $form[static::FIELD_EVENT_TYPE] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Event type'),
      '#default_value' => '',
      '#required'      => TRUE,
      '#options'       => [
        static::EVENT_TYPE_ELEMENT_SHOW => $this->t('On element show.'),
        static::EVENT_TYPE_NO_ERROR => $this->t('On valid form.'),
        static::EVENT_TYPE_ERROR    => $this->t('On errors.'),
      ],
      '#description'   => $this->t('The element must be placed at the end of the form part to be efficient.'),
    ];

    return $form;
  }

}
