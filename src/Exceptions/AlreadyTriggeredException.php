<?php

namespace Drupal\trigger_api\Exceptions;

/**
 * Exception triggered when item is added into an already sent queue.
 */
class AlreadyTriggeredException extends \Exception {

  /**
   * {@inheritdoc}
   */
  public function __construct($message = "", $code = 0, \Throwable $previous = NULL) {
    $message = empty($message) ? "Trigger queue has already been triggered. You cannot add queue items anymore." : $message;
    parent::__construct($message, $code, $previous);
  }

}
