<?php

namespace Drupal\trigger_api\Services;

use Drupal\trigger_api\Data\TriggerQueueDataInterface;

/**
 * Interface for Trigger Queue service.
 */
interface TriggerQueueInterface {

  /**
   * Return the service.
   *
   * @return \Drupal\trigger_api\Services\TriggerQueueInterface
   *   The service.
   */
  public static function me(): TriggerQueueInterface;

  /**
   * Add a trigger data to queue.
   *
   * @param \Drupal\trigger_api\Data\TriggerQueueDataInterface $data
   *   The data.
   */
  public function add(TriggerQueueDataInterface $data): void;

  /**
   * Get serialized queued data.
   *
   * @return array
   *   The serializable data (json way).
   */
  public function getSerializableData(): array;

  /**
   * Get the queued data.
   *
   * @return array
   *   The data.
   */
  public function getData(): array;

  /**
   * Flush the queue.
   */
  public function flush(): void;

}
