<?php

namespace Drupal\trigger_api\Services;

use Drupal\trigger_api\Data\TriggerQueueDataInterface;
use Drupal\trigger_api\Exceptions\AlreadyTriggeredException;

/**
 * Queue of data tirgger to javascript.
 */
class TriggerQueue implements TriggerQueueInterface {

  /**
   * Service ID.
   *
   * @const string
   */
  const SERVICE_NAME = 'trigger_api.trigger_queue';

  /**
   * The Queue.
   *
   * @var \Drupal\trigger_api\Data\TriggerQueueDataInterface[]
   */
  protected array $queue = [];

  /**
   * Status of the queue.
   *
   * @var bool
   */
  protected bool $hasBeenTriggered = FALSE;

  /**
   * {@inheritdoc}
   */
  public static function me(): TriggerQueueInterface {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public function add(TriggerQueueDataInterface $data): void {
    if ($this->hasBeenTriggered) {
      throw new AlreadyTriggeredException();
    }
    $this->queue[] = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getSerializableData(): array {
    return array_map(function ($data) {
      return [
        'type' => $data->getType(),
        'data' => $data->getSerializableData(),
      ];
    }, $this->queue);
  }

  /**
   * {@inheritdoc}
   */
  public function getData(): array {
    return $this->queue;
  }

  /**
   * {@inheritdoc}
   */
  public function flush(): void {
    $this->queue = [];
    $this->hasBeenTriggered = TRUE;
  }

}
