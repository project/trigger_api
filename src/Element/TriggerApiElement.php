<?php

namespace Drupal\trigger_api\Element;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Hidden;
use Drupal\trigger_api\Plugin\WebformElement\WebformTriggerApi;

/**
 * Candidature selector field.
 *
 * @FormElement("webform_trigger_api")
 */
class TriggerApiElement extends Hidden {

  /**
   * The global formstate.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected FormStateInterface $globalFormState;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $values = parent::getInfo();
    $values['#process'][] = [$this, 'process'];
    $values['#post_render'][] = [$this, 'postRender'];

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function process(&$element, FormStateInterface $formState, &$complete_form) {
    $element['#theme'] = "webform_trigger_api_input";
    $element['#trigger_data'] = Json::encode(Json::decode($element['#trigger_data']));
    $this->globalFormState = $formState;

    return $element;
  }

  /**
   * Check errors state.
   *
   * @param string $content
   *   The content.
   * @param array $element
   *   The build array.
   *
   * @return mixed|string
   *   THe value.
   */
  public function postRender($content, array $element) {
    $hasErrors = count($this->globalFormState->getErrors()) > 0;
    $event_types = array_filter($element['#' . WebformTriggerApi::FIELD_EVENT_TYPE]);

    // On show, always trigger element.
    if (in_array(WebformTriggerApi::EVENT_TYPE_ELEMENT_SHOW, $event_types)) {
      return $content;
    }

    // On errors.
    if ($hasErrors && in_array(WebformTriggerApi::EVENT_TYPE_ERROR, $event_types)) {
      return $content;
    }

    // On validate.
    if ($this->globalFormState->isExecuted() && in_array(WebformTriggerApi::EVENT_TYPE_NO_ERROR, $event_types)) {
      return $content;
    }

    return '';
  }

}
