<?php

namespace Drupal\trigger_api\EventSubscribers;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\trigger_api\Ajax\TriggerApiCommand;
use Drupal\trigger_api\Services\TriggerQueueInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber that automatically add TriggerApoCommand in Ajax response.
 */
class ResponseEventSubscriber implements EventSubscriberInterface, TrustedCallbackInterface {

  /**
   * Trigger Queue.
   *
   * @var \Drupal\trigger_api\Services\TriggerQueueInterface
   */
  protected $triggerQueue;

  /**
   * Constructor.
   *
   * @param \Drupal\trigger_api\Services\TriggerQueueInterface $trigger_queue
   *   The trigger queue manager.
   */
  public function __construct(TriggerQueueInterface $trigger_queue) {
    $this->triggerQueue = $trigger_queue;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => ['onResponse', 255],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'onResponse',
    ];
  }

  /**
   * Event trigger on response.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event.
   */
  public function onResponse(ResponseEvent $event) {
    $data = $this->triggerQueue->getData();
    if (!empty($data)) {
      $response = $event->getResponse();
      if ($response instanceof AjaxResponse) {
        foreach ($data as $item) {
          $response->addCommand(new TriggerApiCommand($item));
        }
      }
    }
  }

}
