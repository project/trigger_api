<?php

namespace Drupal\trigger_api\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\filter\Entity\FilterFormat;
use Drupal\trigger_api\Form\Traits\TriggerAPIFormValidatorTrait;

/**
 * Provides a Analytics trigger form.
 */
class TriggerApiCKEditorForm extends FormBase implements BaseFormIdInterface {

  use TriggerAPIFormValidatorTrait;

  /**
   * Field Trigger Type.
   *
   * @const string
   */
  public const FIELD_TRIGGER_TYPE = 'data-trigger-api-on';

  /**
   * Field Type.
   *
   * @const string
   */
  public const FIELD_TYPE = 'data-trigger-api-type';

  /**
   * FIELD data.
   *
   * @const string
   */
  public const FIELD_DATA = 'data-trigger-api-data';

  /**
   * TRIGGER_TYPE APPEAR.
   *
   * @const string
   */
  public const TRIGGER_TYPE_APPEAR = 'appear';

  /**
   * TRIGGER TYPE CLICK.
   *
   * @const string
   */
  public const TRIGGER_TYPE_CLICK = 'click';

  /**
   * TRIGGER TYPE OTHER.
   *
   * @const string
   */
  public const TRIGGER_TYPE_OTHER = 'other';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'trigger_api_trigger_api_ck_editor';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filterFormat = NULL) {
    $inputValues = array_filter($this->getRequest()->query->all());

    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="' . $this->getBaseFormId() . '">';
    $form['#suffix'] = '</div>';

    // FIeld trigger type.
    $form[static::FIELD_TRIGGER_TYPE] = [
      '#type' => 'select',
      '#title' => $this->t('Trigger type'),
      '#options' => $this->getTriggerTypeOptions(),
      '#default_value' => $this->getTriggerTypeDefaultValue($inputValues),
      '#required' => TRUE,
    ];

    // Field trigger type other.
    $state = [
      'select[name="' . static::FIELD_TRIGGER_TYPE . '"]' => ['value' => 'other'],
    ];
    $form[static::FIELD_TRIGGER_TYPE . '_other'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Other'),
      '#states' => [
        'visible' => $state,
        'required' => $state,
      ],
      '#default_value' => $this->getTriggerTypeOtherDefaultValue($inputValues),
    ];

    // TYpe.
    $form[static::FIELD_TYPE] = [
      '#type' => 'textfield',
      '#title' => $this->t('Event type'),
      '#default_value' => $inputValues[static::FIELD_TYPE] ?? '',
      '#required' => TRUE,
    ];

    // Data.
    $form[static::FIELD_DATA] = [
      '#type' => 'textarea',
      '#title' => $this->t('Data'),
      '#element_validate' => [[$this, 'validateJson']],
      '#default_value' => $inputValues[static::FIELD_DATA] ?? '',
      '#required' => TRUE,
    ];

    // Validate.
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    if (count($form_state->getErrors()) === 0) {

      $response->addCommand(new EditorDialogSave($this->getCleanValues($form_state)));
      $response->addCommand(new CloseModalDialogCommand());

    }
    else {
      $response->addCommand(new HtmlCommand('#' . $this->getBaseFormId(), $form));
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'editor_trigger_api_dialog';
  }

  /**
   * Return the list of available trigger types.
   *
   * @return array
   *   The available trigger types.
   */
  protected function getTriggerTypeOptions() {
    return [
      static::TRIGGER_TYPE_APPEAR => $this->t('Appear'),
      static::TRIGGER_TYPE_CLICK => $this->t('Click'),
      static::TRIGGER_TYPE_OTHER => $this->t('Other'),
    ];
  }

  /**
   * Return the trigger type default value.
   *
   * @param array $inputs
   *   The inputs.
   *
   * @return mixed|string
   *   The default trigger type.
   */
  protected function getTriggerTypeDefaultValue(array $inputs = []) {
    $otherValue = $this->getTriggerTypeOtherDefaultValue($inputs);
    if ($otherValue) {
      return static::TRIGGER_TYPE_OTHER;
    }

    return $inputs[static::FIELD_TRIGGER_TYPE] ?? static::TRIGGER_TYPE_APPEAR;
  }

  /**
   * Return the value of other trigger type if not in options list.
   *
   * @param array $inputs
   *   The input values.
   *
   * @return mixed|null
   *   The vaue of other field.
   */
  protected function getTriggerTypeOtherDefaultValue(array $inputs = []) {
    $types = $this->getTriggerTypeOptions();
    if (isset($inputs[static::FIELD_TRIGGER_TYPE]) && !isset($types[$inputs[static::FIELD_TRIGGER_TYPE]])) {
      return $inputs[static::FIELD_TRIGGER_TYPE];
    }

    return NULL;
  }

  /**
   * Return the cleaned values for the ckeditor plugin.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The formState.
   *
   * @return array
   *   The values sent to the ckeditor.
   */
  protected function getCleanValues(FormStateInterface $form_state) {
    // Prepare json data.
    $data = $form_state->getValue(static::FIELD_DATA);
    $data = Json::encode(Json::decode($data));

    return [
      static::FIELD_TYPE => $form_state->getValue(static::FIELD_TYPE),
      static::FIELD_DATA => $data,
      static::FIELD_TRIGGER_TYPE => $form_state->getValue(static::FIELD_TRIGGER_TYPE) === static::TRIGGER_TYPE_OTHER ? $form_state->getValue(static::FIELD_TRIGGER_TYPE . '_other') : $form_state->getValue(static::FIELD_TRIGGER_TYPE),
    ];
  }

}
