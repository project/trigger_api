<?php

namespace Drupal\trigger_api\Form\Traits;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormStateInterface;

/**
 * Validation for API Data format.
 */
trait TriggerAPIFormValidatorTrait {

  /**
   * Validate JSON.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validateJson(array &$element, FormStateInterface $form_state) {
    $no_json = FALSE;
    try {
      if (!Json::decode($element['#value'])) {
        $no_json = TRUE;
      }
    }
    catch (\Exception $e) {
      $no_json = TRUE;
    }

    if ($no_json) {
      $form_state->setError($element, $this->t('The value is not json valid'));
    }
  }

}
