<?php

namespace Drupal\trigger_api\Ajax;

use Drupal\Core\Ajax\CommandInterface;
use Drupal\trigger_api\Data\TriggerQueueDataInterface;

/**
 * Ajax Commands that dispatch trigger api events.
 */
class TriggerApiCommand implements CommandInterface {

  /**
   * Trigger queue data.
   *
   * @var \Drupal\trigger_api\Data\TriggerQueueDataInterface
   */
  protected TriggerQueueDataInterface $triggerQueueData;

  /**
   * Constructor.
   *
   * @param \Drupal\trigger_api\Data\TriggerQueueDataInterface $trigger_queue_data
   *   The trigger queue data.
   */
  public function __construct(TriggerQueueDataInterface $trigger_queue_data) {
    $this->triggerQueueData = $trigger_queue_data;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    return [
      'command' => 'TriggerApiCommand',
      'type'    => $this->triggerQueueData->getType(),
      'data'    => $this->triggerQueueData->getRawData(),
    ];
  }

}
