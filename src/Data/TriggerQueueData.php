<?php

namespace Drupal\trigger_api\Data;

use Drupal\Component\Serialization\Json;

/**
 * Trigger data.
 */
class TriggerQueueData implements TriggerQueueDataInterface {

  /**
   * TYpe.
   *
   * @var string
   */
  protected string $type;

  /**
   * Data.
   *
   * @var array
   */
  protected array $data;

  /**
   * Constructor.
   *
   * @param string $type
   *   The type of trigger.
   * @param array $data
   *   The data.
   */
  public function __construct(string $type, array $data) {
    $this->type = $type;
    $this->data = $data;
  }

  /**
   * {@inheritdoc}
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * {@inheritdoc}
   */
  public function getRawData(): array {
    return $this->data;
  }

  /**
   * {@inheritdoc}
   */
  public function getSerializableData(): array {
    // Trigger json encode exception.
    Json::encode($this->data);
    return $this->data;
  }

}
