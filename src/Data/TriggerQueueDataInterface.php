<?php

namespace Drupal\trigger_api\Data;

/**
 * Describe the data stocked in triggered queue.
 */
interface TriggerQueueDataInterface {

  /**
   * Return the type.
   *
   * @return string
   *   The type.
   */
  public function getType(): string;

  /**
   * Return the raw data.
   *
   * @return array
   *   The data.
   */
  public function getRawData(): array;

  /**
   * Return the serialized data.
   *
   * @return array
   *   The json serializable data.
   */
  public function getSerializableData(): array;

}
