/**
 * @file
 * CKEditor CKEditor plugin.
 *
 * Basic plugin inserting abbreviation elements into the CKEditor editing area.
 *
 * @DCG The code is based on an example from CKEditor Plugin SDK tutorial.
 *
 * @see http://docs.ckeditor.com/#!/guide/plugin_sdk_sample_1
 */

(function (Drupal) {

  'use strict';

  CKEDITOR.plugins.add('trigger_api_ckeditor', {

    // Register the icons.
    icons: 'ckeditor',

    // The plugin initialization logic goes inside this method.
    init: function (editor) {

      // Define an editor command that opens our dialog window.
      // editor.addCommand('ckeditor', new CKEDITOR.dialogCommand('ckeditorDialog'));

      // Create a toolbar button that executes the above command.
      editor.ui.addButton('ckeditor', {

        // The text part of the button (if available) and the tooltip.
        label: Drupal.t('Insert abbreviation'),

        // The command to execute on click.
        command: 'ckeditor',

        // The button placement in the toolbar (toolbar group name).
        toolbar: 'insert'
      });

      // Register our dialog file, this.path is the plugin folder path.
      // CKEDITOR.dialog.add('ckeditorDialog', this.path + 'dialogs/ckeditor.js');

      editor.addCommand('ckeditor', new TriggerAPICKEditorCommandClass('ckeditorDialog'));
    }
  });

}(Drupal));

class TriggerAPICKEditorCommandClass {
  constructor(id) {
    this.id = id;
    // this.allowedContent = this.getAllowedContent()
    // this.requiredContent = this.getRequiredContent()

    this.modes = {wysiwyg: 1}
    this.canUndo = true;
  }

  /**
   * On click on ckeditor plugin button.
   * @param editor
   */
  exec(editor) {

    const dialogSettings = {
      title: 'Trigger API',
      dialogClass: 'editor-trigger-api-dialog'
    };

    const parentItem = editor.getSelection().getStartElement();

    /**
     * Save callback.
     *
     * @param returnValues
     */
    const saveCallback = function (returnValues) {
      editor.fire('saveSnapshot');

      parentItem.setAttribute('data-trigger-api-on', returnValues['data-trigger-api-on'])
      parentItem.setAttribute('data-trigger-api-type', returnValues['data-trigger-api-type'])
      parentItem.setAttribute('data-trigger-api-data', returnValues['data-trigger-api-data'])

      // Save snapshot for undo support.
      editor.fire('saveSnapshot');
    }

    /**
     * Existing values.
     * @type {{"data-trigger-api-on": string, "data-trigger-api-type": string, "data-trigger-api-data": string}}
     */
    if (parentItem) {
      const existingValue = {
        'data-trigger-api-on': parentItem.getAttribute('data-trigger-api-on') || '',
        'data-trigger-api-type': parentItem.getAttribute('data-trigger-api-type') || '',
        'data-trigger-api-data': parentItem.getAttribute('data-trigger-api-data') || '',
      }

      // Open the dialog for the edit form.
      Drupal.ckeditor.openDialog(editor,
        Drupal.url('trigger-api/trigger-api-ckeditor/' + editor.config.drupal.format),
        existingValue,
        saveCallback,
        dialogSettings
      );
    } else {
      alert("You should select an item");
    }
  }
}
