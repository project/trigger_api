import {Plugin} from 'ckeditor5/src/core';
import {Widget} from 'ckeditor5/src/widget';
import InsertTriggerApiDataCommand, {TriggerApiAttrs} from "./insert-trigger-api-data-command";

// cSpell:ignore TriggerApi insertsimpleboxcommand

/**
 * CKEditor 5 plugins do not work directly with the DOM. They are defined as
 * plugin-specific data models that are then converted to markup that
 * is inserted in the DOM.
 *
 * CKEditor 5 internally interacts with simpleBox as this model:
 * <simpleBox>
 *    <simpleBoxTitle></simpleBoxTitle>
 *    <simpleBoxDescription></simpleBoxDescription>
 * </simpleBox>
 *
 * Which is converted for the browser/user as this markup
 * <section class="simple-box">
 *   <h2 class="simple-box-title"></h1>
 *   <div class="simple-box-description"></div>
 * </section>
 *
 * This file has the logic for defining the simpleBox model, and for how it is
 * converted to standard DOM markup.
 */
export default class TriggerApiEditing extends Plugin {
	static get requires() {
		return [Widget];
	}

	init() {
		this._defineSchema();
		this._defineConverters();
		this.editor.commands.add(
			'insertTriggerApiData',
			new InsertTriggerApiDataCommand(this.editor),
		);
	}

	_defineSchema() {
		// Schemas are registered via the central `editor` object.
		const schema = this.editor.model.schema;

		for (const attrName in TriggerApiAttrs) {
			this.editor.model.schema.extend(
				'$text',
				{
					allowAttributes: Object.keys(TriggerApiAttrs)
				});

			this.editor.conversion.for('upcast').attributeToAttribute({
				view: {
					name: 'span'
				},
				model: {
					key: attrName,
					value: viewElement => {
						return viewElement.getAttribute(TriggerApiAttrs[attrName]) || '';
					}
				},
				converterPriority: 'low'
			});

			this.editor.conversion.for('editingDowncast').attributeToElement({
				model: attrName,
				view: (attributeValue, conversionApi) => {
					const attrValues = {};
					attrValues[TriggerApiAttrs[attrName]] = attributeValue;
					return conversionApi.writer.createAttributeElement('span', attrValues, {priority: 5});
				},
				converterPriority: 'low'
			});

			this.editor.conversion.for('dataDowncast').attributeToElement({
				model: attrName,
				view: (attributeValue, conversionApi) => {
					const attrValues = {};
					attrValues[TriggerApiAttrs[attrName]] = attributeValue;
					return conversionApi.writer.createAttributeElement('span', attrValues, {priority: 5});
				}
			});
		}


		schema.register('TriggerApi', {
			// Behaves like a self-contained object (e.g. an image).
			isObject: true,
			// Allow in places where other blocks are allowed (e.g. directly in the root).
			allowWhere: '$block',
		});
	}

	/**
	 * Converters determine how CKEditor 5 models are converted into markup and
	 * vice-versa.
	 */
	_defineConverters() {
		// Converters are registered via the central editor object.
		const {conversion} = this.editor;

	}
}
