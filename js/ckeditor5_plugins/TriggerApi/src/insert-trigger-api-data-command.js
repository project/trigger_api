/**
 * @file defines InsertSimpleBoxCommand, which is executed when the simpleBox
 * toolbar button is pressed.
 */
// cSpell:ignore simpleboxediting

import {Command} from 'ckeditor5/src/core';
import {toMap} from 'ckeditor5/src/utils';
import {findAttributeRange} from 'ckeditor5/src/typing';


export const TriggerApiAttrs = {
	'dataTriggerApiOn': 'data-trigger-api-on',
	'dataTriggerApiType': 'data-trigger-api-type',
	'dataTriggerApiData': 'data-trigger-api-data',
}
export default class InsertTriggerApiDataCommand extends Command {

	execute() {
		const {model} = this.editor;

		model.change((writer) => {
			const defaultValues = this._getDefaultValues(this.editor.model.document.selection);

			this._openDialog(defaultValues);
		});
	}

	refresh() {
		const {model} = this.editor;
		const {selection} = model.document;

		// Determine if the cursor (selection) is in a position where adding a
		// simpleBox is permitted. This is based on the schema of the model(s)
		// currently containing the cursor.
		const allowedIn = model.schema.findAllowedParent(
			selection.getFirstPosition(),
			'TriggerApi',
		);

		// If the cursor is not in a location where a simpleBox can be added, return
		// null so the addition doesn't happen.
		this.isEnabled = allowedIn !== null;
	}

	/**
	 * Open dialog
	 *
	 * @param defaultValues
	 * @private
	 */
	_openDialog(defaultValues) {
		const url = new URL(window.location);
		for (const attrId in defaultValues) {
			url.searchParams.set(attrId, defaultValues[attrId]);
		}

		// Open the dialog for the edit form.
		Drupal.ckeditor5.openDialog(
			Drupal.url('trigger-api/trigger-api-ckeditor?' + url.searchParams.toString()),
			(values) => {
				this.editor.model.change((writer) => {
					const model = this.editor.model
					this._setTriggerAttributes(model.document.selection, writer, values, model);
				});
			},
			{
				title: 'Trigger API',
				dialogClass: 'editor-trigger-api-dialog'
			}
		);
	}

	/**
	 * Get current value from selection.
	 *
	 * @param selection
	 * @returns {{}}
	 * @private
	 */
	_getDefaultValues(selection) {
		const allAttributes = this._getParentTriggerAttributes(selection);

		const attributes = {};
		Object.values(TriggerApiAttrs).map(attr => attributes[attr] = allAttributes[attr] || '');
		return attributes;
	}

	/**
	 * Update content.
	 *
	 * @param selection
	 * @param writer
	 * @param triggerAttributes
	 * @param model
	 * @private
	 */
	_setTriggerAttributes(selection, writer, triggerAttributes, model) {
		const firstAttributeName = Object.keys(TriggerApiAttrs)[0];
		const firstAttributeId = Object.values(TriggerApiAttrs)[0];
		if (selection.isCollapsed) {
			const position = selection.getFirstPosition();
			let elementRange = selection;
			const allAttributes = this._getParentTriggerAttributes(selection);
			elementRange = findAttributeRange(position, firstAttributeName, allAttributes[firstAttributeId], model);

			for (const attrName of Object.keys(TriggerApiAttrs)) {
				writer.setAttribute(attrName, triggerAttributes[TriggerApiAttrs[attrName]], elementRange);
			}
		}
	}

	/**
	 * Return current parent selection trigger attributes.
	 *
	 * @param selection
	 * @returns {{}}
	 * @private
	 */
	_getParentTriggerAttributes(selection) {
		const firstAttributeId = Object.values(TriggerApiAttrs)[0];
		let allAttributes = {};
		const attributesList = toMap(selection.getAttributes());
		for (let [key, value] of attributesList) {
			if (
				typeof value === 'object'
				&& typeof value?.attributes === 'object'
				&& typeof value?.attributes[firstAttributeId] === 'string'
			) {
				allAttributes = value?.attributes;
			}
		}
		return allAttributes;
	}

}
