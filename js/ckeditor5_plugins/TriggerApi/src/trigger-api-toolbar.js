/**
 * @file registers the simpleBox toolbar button and binds functionality to it.
 */

import {Plugin} from 'ckeditor5/src/core';
import {ButtonView} from 'ckeditor5/src/ui';
import icon from '../../../../icons/TriggerApi.svg';

export default class TriggerApiToolbar extends Plugin {
	init() {
		const editor = this.editor;

		editor.ui.componentFactory.add('TriggerApi', (locale) => {
			const command = editor.commands.get('insertTriggerApiData');
			const buttonView = new ButtonView(locale);

			// Create the toolbar button.
			buttonView.set({
				label: editor.t('Trigger API'),
				icon,
				tooltip: true,
			});

			// Execute the command when the button is clicked (executed).
			this.listenTo(buttonView, 'execute', () => editor.execute('insertTriggerApiData'));

			return buttonView;
		});
	}
}
