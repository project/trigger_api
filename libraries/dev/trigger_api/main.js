/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/trigger_api/js/main.js":
/*!************************************!*\
  !*** ./src/trigger_api/js/main.js ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _utils_trigger_api__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./utils/trigger-api */ \"./src/trigger_api/js/utils/trigger-api.js\");\n\n\n(function (Drupal) {\n  // closure\n  'use strict'; // Behaviros.\n\n  Drupal.behaviors.trigger_api = _utils_trigger_api__WEBPACK_IMPORTED_MODULE_0__.TriggerApi;\n})(Drupal);\n\n//# sourceURL=webpack://assets/./src/trigger_api/js/main.js?");

/***/ }),

/***/ "./src/trigger_api/js/utils/trigger-api.js":
/*!*************************************************!*\
  !*** ./src/trigger_api/js/utils/trigger-api.js ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"TriggerApi\": () => (/* binding */ TriggerApi)\n/* harmony export */ });\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }\n\nvar TriggerApiClass = /*#__PURE__*/function () {\n  function TriggerApiClass() {\n    _classCallCheck(this, TriggerApiClass);\n\n    window.triggerApiEnableLog = this.enableLog;\n    window.triggerApiDisableLog = this.disableLog;\n  }\n  /**\n   * Drupal attach.\n   *\n   * @param context\n   * @param settings\n   */\n\n\n  _createClass(TriggerApiClass, [{\n    key: \"attach\",\n    value: function attach(context, settings) {\n      var _this = this;\n\n      // Define AjaxCommand\n      if (Drupal.AjaxCommands && typeof Drupal.AjaxCommands.prototype.TriggerApiCommand === \"undefined\") {\n        Drupal.AjaxCommands.prototype.TriggerApiCommand = function (ajax, response, status) {\n          _this.dispatch(response.type, response.data);\n        };\n      } // Trigger from settings.\n\n\n      if (context === document) {\n        this.triggerFromSettings(settings);\n      } // Trigger from context\n\n\n      this.triggerFromContext(context);\n    }\n    /**\n     * Return the event.\n     *\n     * @param type\n     * @param data\n     * @return {Event}\n     */\n\n  }, {\n    key: \"getEvent\",\n    value: function getEvent(type, data) {\n      var event = new Event(type);\n      event.data = data;\n      return event;\n    }\n    /**\n     * Trigger events from settings.\n     *\n     * @param settings\n     */\n\n  }, {\n    key: \"triggerFromSettings\",\n    value: function triggerFromSettings(settings) {\n      var _this2 = this;\n\n      if (settings.trigger_api) {\n        settings.trigger_api.forEach(function (item) {\n          _this2.dispatch(item.type, item.data);\n        });\n      }\n    }\n    /**\n     * Trigger from context.\n     */\n\n  }, {\n    key: \"triggerFromContext\",\n    value: function triggerFromContext(context) {\n      var _this3 = this;\n\n      context.querySelectorAll('[data-trigger-api-on]').forEach(function (item) {\n        try {\n          var type = item.getAttribute('data-trigger-api-type'),\n              eventType = item.getAttribute('data-trigger-api-on'),\n              data = JSON.parse(item.getAttribute('data-trigger-api-data'));\n\n          switch (eventType) {\n            case 'appear':\n              _this3.dispatch(type, data);\n\n              break;\n\n            default:\n              item.addEventListener(eventType, function () {\n                _this3.dispatch(type, data);\n              });\n              break;\n          }\n        } catch (e) {}\n      });\n    }\n    /**\n     * Dispatch event.\n     *\n     * @param type\n     * @param data\n     */\n\n  }, {\n    key: \"dispatch\",\n    value: function dispatch(type, data) {\n      var event = this.getEvent(type, data);\n      document.dispatchEvent(event);\n\n      if (window.localStorage.getItem('trigger_api_log') === 'true') {\n        console.log(type, data);\n      }\n    }\n  }, {\n    key: \"enableLog\",\n    value: function enableLog() {\n      window.localStorage.setItem('trigger_api_log', JSON.stringify(true));\n      console.log(\"=> TRIGGER API : Logs are enabled\");\n    }\n  }, {\n    key: \"disableLog\",\n    value: function disableLog() {\n      window.localStorage.setItem('trigger_api_log', JSON.stringify(false));\n      console.log(\"=> TRIGGER API : Logs are disabled\");\n    }\n  }]);\n\n  return TriggerApiClass;\n}();\n\nvar TriggerApi = new TriggerApiClass();\n\n//# sourceURL=webpack://assets/./src/trigger_api/js/utils/trigger-api.js?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = __webpack_require__("./src/trigger_api/js/main.js");
/******/ 	
/******/ })()
;