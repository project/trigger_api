/**
 * Vous pouvez surcharger ici la configuration par défaut de chaque tache.
 */
module.exports = {
 "build": {
   "src": "src/",
   "dev": "../dev",
   "prod": "../dist"
 },
//  "scripts": {
//    "ext": "{js,ts}",
//    "src": "js/*.",
//    "dest": "",
//    "watch": "js/**/*."
//  }

};
