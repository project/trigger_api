const {series, parallel} = require('gulp')

const {productionMode} = require('bim-gulp/tasks/build/productionMode')
const {clean} = require('bim-gulp/tasks/clean/clean')
const {scripts} = require('bim-gulp/tasks/scripts/scripts')

// Build en mode dev.
exports.build = series(
    clean,
    // Scripts et css sont indépendants
    scripts,
);

// Build en mode production.
exports.production = series(productionMode, exports.build)
// Alias
exports.prod = exports.production
exports.beforeWatch = exports.build
