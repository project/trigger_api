import {TriggerApi} from "./utils/trigger-api";

(function (Drupal) { // closure
  'use strict';

  // Behaviros.
  Drupal.behaviors.trigger_api = TriggerApi;
}(Drupal));
