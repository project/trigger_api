class TriggerApiClass {

  constructor() {
    window.triggerApiEnableLog = this.enableLog
    window.triggerApiDisableLog = this.disableLog
  }

  /**
   * Drupal attach.
   *
   * @param context
   * @param settings
   */
  attach(context, settings) {
    // Define AjaxCommand
    if( Drupal.AjaxCommands && typeof( Drupal.AjaxCommands.prototype.TriggerApiCommand ) === "undefined" ){
      Drupal.AjaxCommands.prototype.TriggerApiCommand = (ajax, response, status ) => {
        this.dispatch(response.type, response.data);
      }
    }

    // Trigger from settings.
    if (context === document) {
      this.triggerFromSettings(settings);
    }

    // Trigger from context
    this.triggerFromContext(context);
  }

  /**
   * Return the event.
   *
   * @param type
   * @param data
   * @return {Event}
   */
  getEvent(type, data) {
    const event = new Event(type);
    event.data = data;
    return event;
  }

  /**
   * Trigger events from settings.
   *
   * @param settings
   */
  triggerFromSettings(settings) {
    if (settings.trigger_api) {
      settings.trigger_api.forEach(item => {
        this.dispatch(item.type, item.data)
      })
    }
  }

  /**
   * Trigger from context.
   */
  triggerFromContext(context) {
    context.querySelectorAll('[data-trigger-api-on]').forEach(item => {
      try{
        const type = item.getAttribute('data-trigger-api-type'),
          eventType = item.getAttribute('data-trigger-api-on'),
          data = JSON.parse(item.getAttribute('data-trigger-api-data'));

        switch (eventType) {
          case 'appear':
            this.dispatch(type, data)
            break;
          default:
            item.addEventListener(eventType, () => {
              this.dispatch(type, data)
            });
            break;
        }
      }
      catch(e){
      }
    })
  }

  /**
   * Dispatch event.
   *
   * @param type
   * @param data
   */
  dispatch(type, data) {
    const event = this.getEvent(type, data)
    document.dispatchEvent(event)
    if( window.localStorage.getItem('trigger_api_log') === 'true' ){
      console.log(type, data)
    }
  }

  enableLog(){
    window.localStorage.setItem('trigger_api_log', JSON.stringify(true));
    console.log("=> TRIGGER API : Logs are enabled");
  }
en
  disableLog(){
    window.localStorage.setItem('trigger_api_log', JSON.stringify(false));
    console.log("=> TRIGGER API : Logs are disabled");
  }
}

export const TriggerApi = new TriggerApiClass();
